

// Author: Hugo Pretorius
// Assignment: Minor Project – FDWM Assessment 2
// Student ID: STU-00001224
// Date: 2017/02/06
// Ref:
// https://materialdesignicons.com/
// https://fonts.google.com/specimen/Merriweather?selection.family=Merriweather:400,700|Open+Sans:300,400
// https://html5boilerplate.com/


var openNav = document.getElementById('open-nav'),
fullMenu = document.querySelector('.full-menu'),
hideHomeFullMenu = document.querySelector('.home-icon'),
simpleNavList = document.querySelector('.simple-nav-list'),
navGBreadcrumb = document.querySelector('nav.g-breadcrumb');

openNav.addEventListener('click', function(e){
    fullMenu.classList.toggle('hidden');
    hideHomeFullMenu.classList.toggle('g-visible-md-inline');
    simpleNavList.classList.toggle('hidden');
    navGBreadcrumb.classList.toggle('menu-open');
    e.preventDefault();
}, false);
